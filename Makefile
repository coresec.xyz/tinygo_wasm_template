main: clean wasm_exec
	tinygo build -o ./html/wasm.wasm -target wasm -no-debug ./main.go
	cp ./index.html ./html/

wasm_exec:
	cp ./wasm_exec.js ./html/

clean:
	rm -rf ./html
	mkdir ./html